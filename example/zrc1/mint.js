const {BN, Long, bytes, units} = require('@zilliqa-js/util');


async function main() {
    const zilliqa = window.Zeeves;

    // Если пользователь не авторизован - здесь откроется дополнительное окно
    // Если авторизован - то сразу вернется информация об адресе
    const walletInfo = await zilliqa.getSession();
    const address = walletInfo.bech32; // bech32 - adress in format of zil...
    console.log("Your account address is:");
    console.log(`${address}`);
    const myGasPrice = units.toQa('2000', units.Units.Li); // Gas Price that will be used by all transactions

    const nftAddr = ""; // сюда подставляете адрес контракта, который создался в ходе деплоя
    try {
        const contract = zilliqa.contracts.at(nftAddr);
        const callTx = await contract.callWithoutConfirm(
            'Mint',
            [
                {
                    vname: 'to',
                    type: 'ByStr20',
                    value: `${address}`,
                },
                {
                    vname: 'token_uri',
                    type: 'String',
                    value: 'some_token_uri',
                }
            ],
            {
                version: VERSION,
                amount: new BN(0),
                gasPrice: myGasPrice,
                gasLimit: Long.fromNumber(10000),
            }
        );

        // check the pending status
        const pendingStatus = await zilliqa.blockchain.getPendingTxn(callTx.id);
        console.log(`Pending status is: `);
        console.log(pendingStatus.result);

        // process confirm
        console.log(`The transaction id is:`, callTx.id);
        console.log(`Waiting transaction be confirmed`);
        const confirmedTxn = await callTx.confirm(callTx.id);

        console.log(`The transaction status is:`);
        console.log(confirmedTxn.receipt);
    } catch (err) {
        console.log(err);
    }
}

main();
