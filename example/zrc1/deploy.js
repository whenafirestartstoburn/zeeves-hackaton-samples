const fs = require('fs');
const {Long, bytes, units} = require('@zilliqa-js/util');

async function main() {
    const zilliqa = window.Zeeves;

    // Если пользователь не авторизован - здесь откроется дополнительное окно
    // Если авторизован - то сразу вернется информация об адресе
    const walletInfo = await zilliqa.getSession();
    const address = walletInfo.bech32; // bech32 - adress in format of zil...
    console.log("Your account address is:");
    console.log(`${address}`);
    const myGasPrice = units.toQa('2000', units.Units.Li);

    console.log("start to deploy zrc2: ");

    // Здесь код смарт-контракта грузится из файловой системы так как пример
    // был взят из серверного кода 
    // На фронте вероятно понадобится загружать его со ссылки
    const code = fs.readFileSync("nonfungible-token.scilla").toString();
    console.log("contract code is: ");
    console.log(code);
    const init = [
        // this parameter is mandatory for all init arrays
        {
            vname: "_scilla_version",
            type: "Uint32",
            value: "0"
        },
        {
            vname: "contract_owner",
            type: "ByStr20",
            value: `${address}`
        },
        {
            vname: "name",
            type: "String",
            value: `Zeeves x NGS AINFT`
        },
        {
            vname: "symbol",
            type: "String",
            value: `AINFT`
        }
    ];
    console.log("init json is: ");
    console.log(JSON.stringify(init));
    const contract = zilliqa.contracts.new(code, init);
    try {
        const [deployTx, nft] = await contract.deployWithoutConfirm({
            version: VERSION,
            gasPrice: myGasPrice,
            gasLimit: Long.fromNumber(40000)
        }, false);

        if (nft.error) {
            console.error(nft.error);
            return;
        }

		console.log('TXN is:');
		console.log(JSON.stringify(deployTx));		

		console.log("\n\n\nNFT is:");
		console.log(JSON.stringify(nft));
		
        // check the pending status
        const pendingStatus = await zilliqa.blockchain.getPendingTxn(deployTx.id);
        console.log(`Pending status is: `);
        console.log(pendingStatus.result);

        // process confirm

        // Полученный transaction id можно ввести на viewblock.io в виде:
        // https://viewblock.io/zilliqa/tx/0xd371a915e4c07dc3855c4ca7a56b92c1d776ce59a25ecd37bf44f6adf7f31859
        // Там изначально будет отображаться статус Dispatched ....
        console.log(`The transaction id is:`, deployTx.id);
        console.log(`Waiting transaction be confirmed`);

        // Здесь код повиснет вплоть до подтверждения транзакции, которое может занимать до 1 минуты
        const confirmedTxn = await deployTx.confirm(deployTx.id);

        // Introspect the state of the underlying transaction
        console.log(`Deployment Transaction ID: ${deployTx.id}`);

        // Get the deployed contract address
        console.log("The contract address is:");
        console.log(nft.address);
    } catch (e) {
        console.error(e);
    }

}

main();